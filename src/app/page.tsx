import { serverClient } from "./_trpc/serverClient";

import TodoList from "./_components/TodoList";
import Link from "next/link";

export const dynamic = "force-dynamic";

export default async function Home() {
  const todos = await serverClient.getTodos();
  console.log(todos);
  return (
    <main className="max-w-3xl mx-auto mt-5">
      <Link href="/suspense" className="text-black">
        Try Suspense
      </Link>
      <TodoList initialTodos={todos} />
    </main>
  );
}
