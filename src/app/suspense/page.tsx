import Link from "next/link";
import { Client } from "./Client";
import { Suspense } from "react";

export default function Page() {
  return (
    <div>
      <Link href="/" className="text-black">
        Go home
      </Link>
      <Suspense fallback={<p>Loading...</p>}>
        <Client />
      </Suspense>
    </div>
  );
}
