"use client";
import { trpc } from "../_trpc/client";

export const Client = () => {
  const [getTodos, {}] = trpc.getTodos.useSuspenseQuery(undefined, {
    refetchOnMount: false,
    refetchOnReconnect: false,
  });

  return (
    <div>
      <h1>Client</h1>
      <pre className="bg-gray-100 rounded p-1 text-black">
        {JSON.stringify({ todos: getTodos }, null, 2)}
      </pre>
    </div>
  );
};
